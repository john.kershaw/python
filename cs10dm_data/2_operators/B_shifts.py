def shift_left(byte, places=1):
    return byte << places

def shift_right(byte, places=1):
    return byte >> places

assert shift_left(0b1) == 0b10
assert shift_left(0b101) == 0b1010
assert shift_left(0b11) == 0b110
assert shift_left(0b1010) == 0b10100 # ten doubled
assert shift_left(10) == 20   # 10 is 0b1010
assert shift_left(0b1010, 2) == 0b101000 # ten doubled twice
assert shift_left(10, 2) == 40 # ten doubled twice
# assert shift_left(0b???) == 80 # forty doubled
# assert shift_left(0b10001) == 0b??? # 17 doubled

assert shift_right(0b0001) == 0b0000  # rightmost bit drops off (no fractional bits)
assert shift_right(0b101) == 0b10
assert shift_right(0b11) == 0b1
assert shift_right(0b1001) == 0b100 # nine halved
assert shift_right(9) == 4   # 9 // 2 is 4, not 4.5
assert shift_right(14) == 7   
assert shift_right(0b1100) == 0b110


print('All asserts pass')