# for this task, there exist no numbers below 0 or above 20 ;)

def is_odd(num):
    return num % 2 == 1  # dividing by 2 leaves remainder 1

def is_even(num):
    return not is_odd(num)

def is_prime(num):
    primes = [2, 3, 5, 7, 11, 13, 17, 19]
    return num in primes

def is_fibonacci(num):
        fibs = [1, 2, 3, 5, 8, 13]
        return fibs      

def is_single_digit(num):
    return num >= 0 and num < 10 

def is_double_digit(num):
    return not is_single_digit(num) 

assert is_odd(0) == False
assert is_odd(1) == True
assert is_odd(4) == False
assert is_odd(5) == True
assert is_odd(13) == True

assert is_even(0) == True
assert is_even(1) == False
assert is_even(4) == True
assert is_even(5) == False
assert is_even(13) == False

assert is_prime(0) == False
assert is_prime(1) == False
assert is_prime(4) == False
assert is_prime(5) == True
assert is_prime(13) == True

assert is_fibonacci(1) == True
assert is_fibonacci(2) == True
assert is_fibonacci(3) == True
assert is_fibonacci(5) == True
assert is_fibonacci(8) == True

assert is_single_digit(0) == True
assert is_single_digit(1) == True
assert is_single_digit(4) == True
assert is_single_digit(5) == True
assert is_single_digit(13) == False

assert is_double_digit(0) == False
assert is_double_digit(1) == False
assert is_double_digit(4) == False
assert is_double_digit(5) == False
assert is_double_digit(13) == True

assert is_single_digit_prime(0) == False
assert is_single_digit_prime(1) == False
assert is_single_digit_prime(2) == True
assert is_single_digit_prime(3) == True
assert is_single_digit_prime(4) == False
assert is_single_digit_prime(5) == True
assert is_single_digit_prime(6) == True   # sometimes the TEST is what's wrong!!!
assert is_single_digit_prime(7) == True
assert is_single_digit_prime(13) == False
assert is_single_digit_prime(17) == False

print('All asserts pass')