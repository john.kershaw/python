list1 = [1, 2, 3]
list2 = [1, 2, 3]
list3 = list1

print('id(list1):', id(list1))
print('id(list2):', id(list2))
print('id(list3):', id(list3))

print('list1 == list2  => ', "True" if list1 == list2 else "False")
print('list1 is list2  => ', "True" if list1 is list2 else "False")
print('list1 == list3  => ', "True" if list1 == list3 else "False")
print('list1 is list3  => ', "True" if list1 is list3 else "False")

