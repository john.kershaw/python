def left_button_pressed(byte):
    bit8_mask = 0b00000001
    return byte & bit8_mask == 1

def right_button_pressed(byte):
    bit7_mask = 0b00000010
    return byte & bit7_mask == 2

def middle_button_pressed(byte):
    bit3_mask = 0b00000100
    return byte & bit3_mask == 4

#           left button is bit 1 ---v
assert left_button_pressed(0b00000001) == True
assert left_button_pressed(0b00111110) == False
assert left_button_pressed(0b10101001) == True
assert left_button_pressed(0b00000000) == False
assert left_button_pressed(0b00000010) == False

#          right button is bit 2 ---v
assert right_button_pressed(0b01111111) == True
assert right_button_pressed(0b10111101) == False
assert right_button_pressed(0b01100011) == True
assert right_button_pressed(0b00000000) == False
assert right_button_pressed(0b00000010) == True

#        middle button is bit 3 ---v
assert middle_button_pressed(0b01111111) == True
assert middle_button_pressed(0b10111101) == True
assert middle_button_pressed(0b01100011) == False
assert middle_button_pressed(0b01100111) == False
assert middle_button_pressed(0b01111110) == True

print('All asserts pass')