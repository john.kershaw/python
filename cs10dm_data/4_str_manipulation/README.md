## 0. Create a new branch:

* Open gitpod.io/workspaces
* Click your pinned instance (or 'new' and choose your personal repo)
* ctrl+shift+p (=search everywhere)
* Type "git", select "Git Create Branch From..."
* Select mrk_original, then master
* For 'new branch name', type '4_str_manipulation' 
* Click 'Publish', select YOUR origin


## 1. Read/learn/understand these

* 0_string_slicing
* 1_string_methods


## 2. Complete A_string_methods [8]

* Open A_strings_methods
* Delete .??? after "I'm shouting!" (including the dot)
* Type a dot after the " - you should see a list of string methods
* Locate .upper() with arrow keys & select with enter 
* Click 'Commit' then Sync (or Publish if you forgot earlier)
* Repeat for rest of A_string_methods


## 3. Complete B_dna [4]

* Read & run the code, check you understand it 
* Improve lines 14, 16, 18, 20 using the += operator
* Improve the output formatting of lines 23-26


4. Complete C_codons [9]

* Write a body for the function contains_codon so the 2 asserts pass
* Create 3 more amino acid constants similar to ACID_H and ACID_N
* Write 4 more asserts that check for the 5 amino acids in the dna sequence
* TIPS: ctrl+L for 'select line', alt+up/down for move line, Search+F2 to rename