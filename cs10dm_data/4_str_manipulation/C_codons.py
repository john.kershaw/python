dna = "ATCGATGAGAGCTAGCGATA"

# FIXME Use str.count(substr) to return True or False 
def contains_codon(seq: str, codon: str):
    return None

assert contains_codon(dna, 'ATC') == True
assert contains_codon(dna, 'TGT') == False


# File codon_wheel.png shows all possible codon triplets
# To decode a codon find the first letter of your sequence 
# in the inner circle and work outwards to see the corresponding
# amino acid. For example: CAT codes for H (Hisitidine) 

# Create 3 more acids using the codon_wheel.png
ACID_H = 'CAT'
ACID_N = 'AAC'


# Add 4 more asserts that check for the 3-letter codons you just created
# in the 'dna' sequence
assert contains_codon(dna, ACID_H) == False

if contains_codon(dna, 'ATG'):
    print ("A start codon (ATG) appears in the DNA sequence.")
else:
    print ("A start codon (ATG) does not appear in the DNA sequence.")

print('All asserts pass')
