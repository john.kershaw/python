# Replace ??? with the correct string methods to make the asserts pass

assert "I'm shouting!".??? == "I'M SHOUTING!"
assert "expand\tmy\ttabs".??? == 'expand  my      tabs'
assert "this is great".??? == 'This is great'
assert "I live in Southend".???('end') == True
assert "1234567".??? == True
assert "     Strip my whitespace!        ".??? == "Strip my whitespace!"
assert "I'm a dog person, I like dogs.".??? == "I'm a cat person, I like cats."
assert "images/logos/nike.png".???('/') == ["images", "logos", "nike.png"]

print('All asserts pass')