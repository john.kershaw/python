# Given a DNA sequence, find the percentages of 
# A, C, T & G nucleotides in the sequence

dna = "ATCGATGAGAGCTAGCGATAATC"

a_count = 0
c_count = 0
t_count = 0
g_count = 0

# Use += to simplify these assignments
for letter in dna:
    if letter == 'A':
        a_count = a_count + 1
    elif letter == 'C':
        c_count = c_count + 1
    elif letter == 'T':
        t_count = t_count + 1
    elif letter == 'G':
        g_count = g_count + 1

# Add  :0.2f before } to display 2 decimal places
print (f"As in sequence: { float(a_count) / len(dna) * 100 }%")
print (f"Cs in sequence: { float(c_count) / len(dna) * 100 }%")
print (f"Ts in sequence: { float(t_count) / len(dna) * 100 }%")
print (f"Gs in sequence: { float(g_count) / len(dna) * 100 }%")
