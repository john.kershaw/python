print("Haha!".lower())
print("Haha!".upper())
print("Haha!".count("ha"))
print("Haha!".lower().count("ha"))
print("fishcakes".find('a'))
print("/".join(["My Drive", "Homework", "English", "Year 10"]))
print("How now brown cow?".split())
print("         How now brown cow?       ".strip())

# Position your cursor after the second " and press . 
# to see the full list of string methods in Python.
# Use the up/down arrows to view each method's DocString
"any string"