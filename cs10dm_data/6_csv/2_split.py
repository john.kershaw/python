filepath = 'cs10dm_data/6_csv/0_sample.csv'  # path to the file

with open(filepath) as f:  
    text = f.read()

# A file ending .csv (comma separated values) contains rows of data
# We can use .split() to convert this to a list.
# Use .strip() first to remove any extra returns at the end, 
# otherwise the list will contain empty elements
rows = text.strip().split('\n')
print(rows)