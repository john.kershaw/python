# Since a csv file contains comma separated values, we can split rows 
# on commas to extract individual values.
filepath = 'cs10dm_data/6_csv/0_sample.csv'  # path to the file

with open(filepath) as f:  
    text = f.read()

rows = text.strip().split('\n')


# The first row will contain the headers - Name, Maths, English, and Science
fields = rows[0].split(',')
print(fields)

#The next row will contain information on the first student, Anne.
student = rows[1].split(',')
print(student)

# Now we can create a loop that displays Anne's information in an easily readable fashion: 
for i in range(len(fields)):
    print(fields[i], student[i])