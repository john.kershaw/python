filepath = 'cs10dm_data/6_csv/0_sample.csv'  # path to the file

# If we forget to close the file, no-one else can access it!
# If our Python program crashes before we close, same problem!

# A 'with' ensures the file will be always be closed after use:
with open(filepath) as f:  
    text = f.read()

print(text)