# First we need the path to the file we want to read, starting from top
filepath = 'cs10dm_data/6_csv/0_sample.csv'  # path to the file

# To read from a file you need to OPEN, READ, CLOSE:
f = open(filepath)     # ask file system for a 'file handler id'  
text = f.read()        # grab all the text from the file
f.close()              # tell the file system we're done with the file

print(f)               # the 'file handler' id
print(text)            # the contents of the file