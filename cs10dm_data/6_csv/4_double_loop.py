filepath = 'cs10dm_data/6_csv/0_sample.csv'  # path to the file

with open(filepath) as f:  
    text = f.read()

rows = text.strip().split('\n')
fields = rows[0].split(',')

# loop through rows, print field & value
for row in rows[1:]:
    student = row.split(',')
    for i in range(len(fields)):
        print(fields[i], student[i])
    print()