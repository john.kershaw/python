# Write your code below each step. 
# Comments = your instructions AND my mark scheme, so don't delete them!
# Uncomment asserts to check your progress
# TIP: Move lines up/down with alt+up/down


# 1   ITERATE OVER LINES        /4

# 1a. Create a variable 'filepath', set to 'cs10dm_data/6_csv/A_khan.csv'
# 1b. Read khan.csv into a string called 'text'
# 1c. Use .strip to remove any blank lines.
# 1d. Convert to a list called 'lines' (see 2_split.py for how) 
# assert len(lines) == 15


# 2   OUTPUT STUDENT            /5

# 2a. Iterate over the list with a loop eg 'for line in lines[1:]'
# 2b. Output each student's whole line
# 2c. Chop line at commas: values = line.split(',')
# 2d. Output just the name of each student 
# 2e. Output name with score, eg 'Brian has completed 55%'
    # assert values[-1] in ['Completed', 'Not started']


# 3   STORE STUDENTS IN LIST      /3

# 3a. Before the loop, initialise an empty list called 'students'
# 3b. Inside the loop, students.append(line.split(",")) to store scores
# 3c. After the loop, print out students[0][0]
#assert students[0][0] == 'Angela', 'First student should be Angela'


# 4   STUDENTS to GRAPH     /5

# 4a. Create a new loop that iterates over the new 'students' list
# 4b. Create variable 'out_of_ten'. Set it to their score DIV 10
# 4c. Create variable 'stars'. Set it to between 0 and 10 stars,
#     eg Angela scored 100, so '**********' but Freda scored 24 so '**'
# 4d. Output eg "Freda **" for each student
# 4e. Calculate how many spaces are needed to make the name up to len 10
#     Add spaces after each name so the **** bars line up, creating a graph:

# Angela     **********
# Brian      ******
# Cherie     ****
# Damien     ***
# Edward     *
# etc
# assert stars == 3, 'Neil has 30, so 3 stars should be the last value'


# 5   ITERATE OVER FIELDS   /4

# 5a. Create a list of the field names: fields = lines[0].split(",")
# assert fields[0] == 'Student name'
# assert fields[1] == 'Percent completed'

# 5b. Create a new loop that iterates over 'fields'
# 5c. Output the field eg 'Article: Ask for help'
# 5d. ONLY output the field if its first 4 letters are in ['Chal', 'Proj']
# assert fields[-1] == 'Article: What to learn next'


'''
Tasks 6 & 7 will create a finished summary report that looks like this:

Neil (20%)
==========
Project: What's for dinner? - completed
Project: Shooting star - completed
Project: Animal attack - not started
Project: Ad design - not started
Project: Fish tank - not started
Project: Magic 7-Ball - not started
Project: Build-a-House - not started
Project: Make it rain - not started
Project: Bookshelf - not started
'''


# 6   CREATE A HEADER         /5

# 6a. Create a(nother) new 'students' loop:
# 6b. Store first 2 values into 2 vars: name,percent = student[0:2]
# 6c. Output a blank line then:  print(        f'{name} ({percent}%)')
# 6d. Capture the header string: print(header:=f'{name} ({percent}%)')
# 6e. Underline the header with an equal number of ==== symbols


# 7   OUTPUT FIELD & STATUS  /6

# 7a. Still INSIDE the students loop, add an inner loop 'i' from 0 to
#     the number of items in 'fields': 
    # for i in range(len(fields)):

# 7b. Use title = fields[i] to extract eg Article: Ask for help
# 7c. Skip any titles that don't start with 'Project'
# 7d. Fix doubled up 'Project: Project: Ad design' weirdness
# 7e. Use status = student[i] to extract their 'Completed/Not started'
# 7f. f-string them nicely, eg Project: Fish Tank - not started

print('All asserts pass')
