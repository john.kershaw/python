filepath = 'cs10dm_data/5_basic_files/2_words.txt'

# output words in reverse order:
words = []
with open(filepath) as f:
    for line in f.readlines():
        words.append(line.strip())
words.sort()
print("\n".join(words))


# find words with same first & last letter, eg pump, tilt, etc 
with open(filepath) as f:
    for line in f.readlines():
        word = line.strip()
        if word:   # ignore blank lines
            if word[0] == word[3]:
                print(word)

