# In Python we can either ASSIGN a value to x:
x = 2    # returns 'None'

# Or OUTPUT the current value of x:
print(x)

# But you can't assign AND output (okay in C, Java, etc):
# print(x = 2)   # often problematic

# Python's 'walrus' operator := lets you do both SAFELY:
print(x := 3)  # set x to 3 AND output it

# To ensure you can never use := when you meant to use
# = or ==, you often need to wrap the walrus in brackets 
# x := 3  # can't do this (you should just use x = 3)
(x := 3)  # this is allowed


# Without the walrus operator, input loops require 
# duplication of the input prompt:
total = 0
num = 0

score = input('Enter a score: ')
while score:    # loop until score is '' (False)
    total += int(score)
    num += 1
    score = input('Enter a score: ')
print(f'average = {total / num}')


# The walrus := lets you move the input into the while condition
# This makes the loop's purpose clear, even when it's folded
total = 0
num = 0

while score := input('Enter a score: '):
    total += int(score)
    num += 1
    score = input('Enter a score: ')
print(f'average = {total / num}')

# Read more: https://realpython.com/python-walrus-operator