**After you Publish/Sync, check you can see your changes on gitlab, then copy the address, +ADD on Classroom, paste the address.**

## 0. Create a new branch:

* Open gitpod.io/workspaces
* Click your pinned instance (or 'new' and choose your personal repo)
* ctrl+shift+p (=search everywhere)
* Type "git", select "Git Create Branch From..."
* Select mrk_original, then master
* For 'new branch name', type '5_FILES' 
* Click 'Publish', select YOUR origin


## 1. Complete A_walrus.py [1]

* Read & run 0_walrus.py
* Open A_walrus.py
* Read the comments
* Do item 1. 
* Click 'Commit' then Sync (or Publish if you forgot earlier)


## 2. Complete B_average.py [9]

* Read & run 1_average.py
* Read B_average.py
* Do items 1-4. Commit & Sync after each


## 3. Complete C_words.py [2]

* Read & run 2_read_file.py
* Read & run C_words.py
* Do items 1 & 2. Commit & Sync after each

TIPS: ctrl+K ctrl+S (ie hold ctrl, tap K then S, release ctrl) for 'Show keyboard shortcuts'.