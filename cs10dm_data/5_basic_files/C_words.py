filepath = 'cs10dm_data/5_basic_files/2_words.txt'

# find words with the pattern A__A, ie same first & last letter, 
# eg pump, tilt, etc
A__A = []
with open(filepath) as f:
    for line in f.readlines():
        word = line.strip()
        if word:   # ignore blank lines
            if word[0] == word[3]:
                A__A.append(word)
print(A__A)

assert 'pump' in A__A
assert 'tilt' in A__A
assert 'hash' in A__A
assert 'pomp' in A__A

# 1. Use a walrus := to combine lines 8 & 9 ie merge 'word = ...' with 'if word[0]...' 

# 2. Modify 'if word[0] == ...' to find words matching pattern ABBA, 
# eg sees, maam, noon, etc
ABBA = []
with open(filepath) as f:
    for line in f.readlines():
        word = line.strip()
        if word:   # ignore blank lines
            if word[0] == word[3]:
                ABBA.append(word)
print(ABBA)

assert 'sees' in ABBA
assert 'toot' in ABBA
assert 'deed' in ABBA
assert 'peep' in ABBA
assert 'tilt' not in ABBA
assert 'hash' not in ABBA

print('All asserts pass')
