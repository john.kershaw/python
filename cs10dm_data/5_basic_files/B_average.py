import random

# 1. Change this list of 100 6-sided dice rolls to use 4-sided dice:
nums = random.choices(range(1,7), k=100)
print(nums)

# 2. Create a list of unique numbers using list(set(nums)), then sort them:
unique_nums = list(set(nums))
unique_nums.sort()    # doesn't return a value, just sorts the list 'in-place'
print(unique_nums)
assert unique_nums == [1, 2, 3, 4]


#################
# MODAL AVERAGE #
#################

# Loop through unique_nums and check if this side was rolled more times
# than the current highest rolled side so far: 
max_rolls = 0
max_side = None
for i in unique_nums:
    print(f'{i}s in nums: {nums.count(i)}')
    if nums.count(i) > max_rolls:
        max_side = i
        max_rolls = nums.count(i)
        print(f'mode is now {max_side}')

# 3a. Use a walrus inside the print to store the value of nums.count(i) eg
#     {(rolls := nums.count(i))}   
#     Notice you need to wrap the walrus in an extra set of (...)

# 3b. Replace the other two occurrences of nums.count(i) with rolls


##################
# MEDIAN AVERAGE #
##################

# 4. Use a walrus := inside this median function to store (len(nums)) into how_many
#    then replace the other 3 uses of this expensive function call with how_many

def median(nums):
    nums.sort()   # median requires list be sorted from smallest to largest
    if (len(nums)) % 2 == 1:        # if odd number of items...
        med = nums[len(nums) // 2]  #    ...take the middle value
    else:                           # otherwise average the two middle values
        med = (nums[len(nums) // 2 - 1] + nums[len(nums) // 2]) / 2
    return med

assert median([1, 2, 3]) == 2
assert median([3, 1, 2]) == 2
assert median([1, 2, 3, 4]) == 2.5
assert median([1, 2, 3, 4, 5]) == 3
assert median([1, 2, 3, 4, 5, 6]) == 3.5
assert median([1]) == 1

print('All asserts pass')