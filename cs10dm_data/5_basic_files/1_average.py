##############################
# CALCULATE THE MEAN AVERAGE #
##############################

import random

# make a list of 100 random numbers 1-10
nums = random.choices(range(1,11), k=100)
print(nums)


################
# USING A LOOP #
################

# count how many numbers, and sum their values:
f = 0
fx = 0
for n in nums:
    f += 1
    fx += n

print(f'{fx} / {f} = {fx / f}')


#####################
# USING LEN AND SUM #
#####################

# Python has built-in functions for
# counting and summing a list of numbers:
f = len(nums)
fx = sum(nums) 
print(f'{fx} / {f} = {fx / f}')


####################
# USING THE WALRUS #
####################

# Since the two summing variables are temporaries
# (they're only used this one time), their
# definitions are cluttering up two lines of code

# We can use the walrus := to create the vars inside the print statement, 
# emphasising their temporary nature
print(f'{(fx:=sum(nums))} / {(f:=len(nums))} = {fx / f}')

