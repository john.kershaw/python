# Here's an expensive function we'd rather not call multiple times
def quad(x):
    return x**2 - 5*x + 5

# This loop calls quad(x) TEN times - ouch!
y_values = []
for x in range(10):
    y_values.append(quad(x))

print(y_values)
assert y_values == [5, 1, -1, -1, 1, 5, 11, 19, 29, 41]  


# If we use an 'if' clause to skip negative outputs... we call quad
# twice as many times - that's TWENTY calls. 
# Imagine if we looped 1000 times...!
expensive_y_values = []
for x in range(10):
    if quad(x) > 0:
        expensive_y_values.append(quad(x))

print(expensive_y_values)
assert expensive_y_values == [5, 1, 1, 5, 11, 19, 29, 41]  # no negatives - yay!

# 1. Use if (y := quad(x)) to capture the returned value from quad(x), 
# then replace quad(x) in the 'append' with y, to avoid double-calling quad(x)
cheap_y_values = []
for x in range(10):
    if quad(x) > 0:
        cheap_y_values.append(quad(x))

print(cheap_y_values)
assert cheap_y_values == [5, 1, 1, 5, 11, 19, 29, 41]

print('All asserts pass')