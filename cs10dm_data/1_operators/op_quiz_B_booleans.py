# BOOLEANS QUIZ

# UNCOMMENT the first assert (ctrl+/)
# REPLACE ? with True or False
# RUN the code to check all asserts pass
# MOVE to the next assert

# expects 4 True/False values for rain, out, etc
def is_wet(rain, out, brolly, coat):
    return rain and out and not (brolly or coat)


# example - caught out in the rain:
assert is_wet(rain=True, out=True, brolly=False, coat=False) == True


# check you are NOT wet if it's not raining
#assert is_wet(rain=?, out=False, brolly=False, coat=False) == False

# check you are NOT wet if you're inside
#assert is_wet(rain=True, out=?, brolly=False, coat=False) == False

# check you are NOT wet if you are protected
#assert is_wet(rain=True, out=True, ???, ???) == False

# check you ARE wet if you are "out innit wi'out owt"
#assert is_wet(???, ???, ???, ???) == True

print('All asserts pass')