# ARITHMETIC QUIZ

# UNCOMMENT the first assert (ctrl+/)
# REPLACE ? with the correct operator
# RUN the code to check all asserts pass
# MOVE to the next assert


# example
assert 100 / 10 == 10.0


# assert 25 / 2 == 12.5
# assert 25 ? 2 == 12
# assert 25 ? 2 == 50
# assert 25 ? 2 == 27
# assert 25 ? 2 == 625
# assert 25 ? 2 == 23
# assert 25 ? 0.5 == 5
# assert 10 ** (9 ? 0.5) == 1000

print('All asserts pass')