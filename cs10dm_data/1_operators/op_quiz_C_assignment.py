# ASSIGNMENT QUIZ

# UNCOMMENT a pair of ?? and assert lines (ctrl+/)
# REPLACE ?? with the correct operator
# RUN the code to check all asserts pass
# MOVE to the next assert

a = 10
b = 2
c = 4


# example: add 1 to c, store result in c 
c += 1
assert c == 5, "c should now be 5"


# a ?? 2
# assert a == 12

# a ?? b
# assert a == 6

# a ?? c
# assert a == 1

print('All asserts pass')

