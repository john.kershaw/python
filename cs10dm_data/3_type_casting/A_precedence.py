# Make all the asserts pass

def is_working(state):
    taxable_states = ['self-employed', 'employed']
    return None
assert is_working('self-employed') == True
assert is_working('employed') == True
assert is_working('worker') == True
assert is_working('unemployed') == False
assert is_working('retired') == False
assert is_working('ill') == False
assert is_working('disabled') == False

def is_working_age(age):
    ADULT_AGE = 18
    PENSION_AGE = 66
    return None
assert is_working_age(17) == False
assert is_working_age(18) == True
assert is_working_age(65) == True
assert is_working_age(66) == False


def is_over_tax_limit(income):
    return None
assert is_over_tax_limit(12569) == False
assert is_over_tax_limit(12570) == False
assert is_over_tax_limit(12571) == True

# Use the three functions to improve this code
# without changing its meaning or output

age = 37
employment = 'employed'
income = 21000
if age > 18 and age < 65 and employment in ['employed', 'worker'] and income > 12570:
    print('You must pay tax')
else: 
    print('You do not have to pay tax')

