# Python operators have 19 levels of 'precedence' to determine
# which operators get done first, like B I DM AS, in maths

# #     Operator                Description
# ---------------------------------------------------
# 1     :=                      Assignment expression
# 2     lambda                  Lambda expression
# 3     if-else                 Conditional expression
# 4     or                      Boolean OR
# 5     and                     Boolean AND
# 6     not x                   Boolean NOT
# 7     <, <=, >, >=,           Comparison operators
# 8     !=, ==                  Equality operators
# 9     in, not in, is, is not  Identity operators, membership operators
# 10    |                       Bitwise OR
# 11    ^                       Bitwise XOR
# 12    &                       Bitwise AND
# 13    <<, >>                  Left and right Shifts
# 14    +, –                    Addition and subtraction
# 15    @, *, //, /, %          (Matrix) Multiplication, (Floor) Division, Remainder
# 16    +x, -x, ~x              Unary plus, Unary minus, bitwise NOT
# 17    **                      Exponentiation (powers, indices, square root, etc)
# 18    await x                 Await expression
# 19    x[i], x(args), x.a      Subscript, slice, call, attributes & methods
# 20    ()                      Parentheses  (Highest precedence)



# Same level operators (like divide & multiply) are applied from left to right

# brackets around 1/2 forces / to evaluate before ** (power) or negation (-)
x = 5 > 16 ** -(1/2) and chr(64 + ('i' in 'fish'[1:3]))  
print(x)


# Step by step explanation:

# 1. 'fish'[1:3] extracts characters from 1 to 3, starting at 0, giving 'is'
x = 5 > 16 ** -0.5   and chr(64 + ('i' in 'is'))  


# 2. The unary operator '-' converts 0.5 into a negative
# 3. The string 'is' DOES contain an 'i', so evaluate True
x = 5 > 16 ** -0.5 and chr(64 + True)


# 4. The square root of 16 is 4; the reciprocal (flipped fraction) of 4 is a quarter
# 5. True is silently converted to 1 so it can be added to 64
x = 5 > 0.25 and chr(65)


# 6. chr(65) returns ASCII letter 'A'
x = True and 'A'

# 7. Left side evaluates to Boolean (True) so the 'and' returns the right side
x = 'A'                 

# Final value!
print(x)


# HOWEVER... 
# if it takes a precedence chart with 20 levels to figure out what Python will do, 
# that's probably (definitely) too clever for humans to get right :(

# Better options:
# 
# 1. Use (brackets) to clarify/enforce your expected order:

x = 5 > (16 ** -(1/2)) and (chr(64 + ('i' in 'fish'[1:3])))
print(x)


# 2. Create temporary variable(s):

flipped_root = 16 ** -(1/2)
letter = chr(64 + ('i' in 'fish'[1:3]))
x = 5 > flipped_root and letter
print(x)


# 3. Move the logic into its own function with an easily understood name:

def flipped_sqrt(num):
    return 1/(num ** 0.5)

def alpha_letter(num):
    return chr(64 + num)

x = 5 > flipped_sqrt(16) and alpha_letter('i' in 'is')
print(x)

# BONUS: Add asserts to make darned sure the result is what you expect!
assert flipped_sqrt(16) == 0.25
assert flipped_sqrt(100) == 0.1
assert alpha_letter(1) == 'A'
assert alpha_letter(26) == 'Z'