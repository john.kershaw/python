# all these are False
print(bool(0))
print(bool(0.0))
print(bool(''))
print(bool([]))
print(bool(False))
print(bool(None))

# all these are Truth
print(bool(3))
print(bool(-4.7))
print(bool('False'))
print(bool([0]))
print(bool(True))

# Shortcuts
# in a 'boolean context', eg an if condition, 
# anything that CAN evaluate to True or False 
# will be silently converted before comparison:

name = input('Name: ')
if name:  # the empty string silently converts to False
    print(f'Great to meet you {name}')
else:
    print('Great to meet you, Anonymous Visitor')