# as SQL
import sqlite3

# create a database connection & a 'cursor'
con = sqlite3.connect("cars.db")
con.row_factory = sqlite3.Row
cur = con.cursor()

# get list of sql tables
res = cur.execute("SELECT name FROM sqlite_master")
tables = res.fetchone()

# delete existing 'cars' if it exists
if 'cars' in tables:
    cur.execute('DROP TABLE cars')    

# if 'cars' doesn't exist yet, create it
cur.execute('CREATE TABLE cars(reg_plate, make, model, price, engine_size, petrol)')

cars_csv = """
reg_plate,make,model,price,engine_size,petrol
VK134KE,Fiat,Punto,14000,1.2,True
AB123YZ,Ford,Galaxy,20000,1.8,True
""".strip()

# convert csv text to a list of lists
data = []
for line in cars_csv.split('\n')[1:]:
    values = line.split(',')
    data.append( values ) 

sql = 'INSERT INTO cars VALUES (?, ?, ?, ?, ?, ?)' 
cur.executemany(sql, data)  # send statement to db
con.commit()      # commit changes to db

result = cur.execute('''
SELECT make, model, reg_plate 
FROM cars
''')
for car in result:
    print(f"The reg of the {car['make']} {car['model']} is {car['reg_plate']}")
