# as class


class Car:
  def __init__(self, values):
   self.reg_plate = values[0]
   self.make = values[1]
   self.model = values[2]
   self.price = values[3]
   self.engine_size = values[4]
   self.petrol = values[5]

cars_csv = """
reg_plate,make,model,price,engine_size,petrol
VK134KE,Fiat,Punto,14000,1.2,True
AB123YZ,Ford,Galaxy,20000,1.8,True
""".strip()

lines = cars_csv.split('\n')[1:]
print(lines)

cars = []   # empty list to hold ALL the car objects
for line in lines:
    values = line.split(',') 
    car = Car(values)
    cars.append(car)

for car in cars:
    print(f"The reg of the {car.make} {car.model} is {car.reg_plate}")
