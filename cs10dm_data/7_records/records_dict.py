# as dict
cars_csv = """
reg_plate,make,model,price,engine_size,petrol
VK134KE,Fiat,Punto,14000,1.2,True
AB123YZ,Ford,Galaxy,20000,1.8,True
""".strip()
fields = cars_csv.split('\n')[0].split(',')
print(fields)

lines = cars_csv.split('\n')[1:]
print(lines)

cars = []   # empty list to hold ALL the cars
for line in lines:
    car = {}   # empty dict for 1 car
    values = line.split(',') 
    for i,field in enumerate(fields):  # 0,reg_plate 1,make etc
        car[field] = values[i]
    print(car)
    cars.append(car)

print(f"The reg of the {cars[0]['make']} {cars[0]['model']} is {cars[0]['reg_plate']}")
