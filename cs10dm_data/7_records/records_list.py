# as list
cars_csv = """
reg_plate,make,model,price,engine_size,petrol
VK134KE,Fiat,Punto,14000,1.2,True
AB123YZ,Ford,Galaxy,20000,1.8,True
""".strip()
car_fields = cars_csv.split('\n')[0]
print(car_fields)

cars = cars_csv.split('\n')[1:]

print(cars[0])

cars[0] = cars[0].split(',')
print(cars[0])

print(f"The reg of the {cars[0][1]} {cars[0][2]} is {cars[0][0]}")

