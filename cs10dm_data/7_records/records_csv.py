# as plaintext
cars_csv = """
reg_plate,make,model,price,engine_size,petrol
VK134KE,Fiat,Punto,14000,1.2,True
AB123YZ,Ford,Galaxy,20000,1.8,True
""".strip()
car_fields = cars_csv.split('\n')[0]
print(car_fields)

car0 = cars_csv.split('\n')[1]
car1 = cars_csv.split('\n')[2]

print(car1)
print(f"The reg of the {car1.split(',')[1]} {car1.split(',')[2]} is {car1.split(',')[0]}")
